# Chrono Vector

This project is devoted to studying how word embeddings of words in different time periods reflect semantic changes in the meaning of words.

Inspired by: Diachronic Word Embeddings Reveal Statistical Laws of
Semantic Change (https://arxiv.org/pdf/1605.09096v6.pdf)