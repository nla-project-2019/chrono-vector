from typing import List, Tuple, Dict, Any
import gensim
import scipy
from scipy import stats
from scipy import spatial
import numpy as np
from tqdm import tqdm, tqdm_notebook


def init_w2v_from_ndarray(embeddings: np.ndarray, vocab: Dict[str, int]) -> gensim.models.Word2Vec:
    """

    @return:
    @param embeddings: numpy array with embeddings, where jth row is the embedding for the jth word in vocab
    @param vocab: dict that maps words to indices in embeddings

    example usage:
    init_w2v_from_ndarray(np.array([[1.0, 0.], [0.0, 1.0]]), {'from': 0, 'to': 1})
    """
    k = [list(vocab.keys())]
    w2v = gensim.models.Word2Vec(sentences=k, size=embeddings.shape[1], window=1, min_count=1)
    for key, value in vocab.items():
        w2v.wv.syn0[w2v.wv.vocab[key].index] = embeddings[value, :] / np.linalg.norm(embeddings[value, :])
    w2v.wv.syn0norm = None
    w2v.wv.init_sims()
    return w2v


def orthogonal_procrustes(from_model: gensim.models.Word2Vec, to_model: gensim.models.Word2Vec) -> gensim.models.Word2Vec:
    """

    @param from_model: reference w2v model
    @param to_model: w2v model to align
    @return: the aligned gensim model
    @summary performs the procrustes alignment for two w2v models (https://en.wikipedia.org/wiki/Orthogonal_Procrustes_problem)
    """
    from_model.wv.init_sims()
    to_model.wv.init_sims()
    from_intersected, to_intersected = vocab_intersection(from_model, to_model)
    base_vecs = from_intersected.wv.syn0norm
    other_vecs = to_intersected.wv.syn0norm
    m = other_vecs.T.dot(base_vecs)
    u, _, v = np.linalg.svd(m)
    orthogonal_rotation = u.dot(v)

    to_model.wv.syn0norm = to_model.wv.syn0 = (to_model.wv.syn0norm).dot(orthogonal_rotation)
    return to_model


def vocab_intersection(model1: gensim.models.Word2Vec, model2: gensim.models.Word2Vec) -> Tuple[gensim.models.Word2Vec, gensim.models.Word2Vec]:
    """

    @param model1: the reference model
    @param model2: the model that will be aligned
    @return: the models with the intersected vocabs
    @summary: This function intersects the vocabularies of two gensim models

    """
    vocab_m1 = set(model1.wv.vocab.keys())
    vocab_m2 = set(model2.wv.vocab.keys())

    common_vocab = vocab_m1 & vocab_m2
    common_vocab = list(common_vocab)
    common_vocab.sort(key=lambda w: model1.wv.vocab[w].count + model2.wv.vocab[w].count, reverse=True)

    for model in [model1, model2]:
        indices = [model.wv.vocab[w].index for w in common_vocab]
        old_arr = model.wv.syn0norm
        new_arr = np.array([old_arr[index] for index in indices])
        model.wv.syn0norm = model.wv.syn0 = new_arr
        model.wv.index2word = common_vocab

        old_vocab = model.wv.vocab
        new_vocab = {}

        for new_index, word in enumerate(common_vocab):
            old_vocab_obj = old_vocab[word]
            new_vocab[word] = gensim.models.word2vec.Vocab(index=new_index, count=old_vocab_obj.count)
        model.wv.vocab = new_vocab

    return (model1, model2)


def align_embeddings(models: List[gensim.models.Word2Vec]):
    """

    @param models: a list of gensim w2v models to align.
    @summary: updates embeddings in models via orthogonal procrustes so that they are aligned to the first model.

    example usage:
    align_embeddings([model1, model2, model3])
    """
    for i in range(1, len(models)):
        models[i] = orthogonal_procrustes(models[i-1], models[i])
    return models



def semantic_displacement(models: List[gensim.models.Word2Vec], word: str) -> List[float]:
    """

    @param models: list of gensim w2v models.
    @param word: the target word to calculate semantic displacement.
    @return: list of cosine distances between the embeddings of the target word in different models.
    @summary: calculates the semantic displacement values of the target word at different times.

    example usage:
    semantic_displacement([model1, model2, model3], 'word')
    """
    timesteps = [model[word] for model in models]
    return [scipy.spatial.distance.cosine(timesteps[i - 1], timesteps[i]) for i in range(1, len(timesteps))]


def pairwise_similarity_timeseries(models: List[gensim.models.Word2Vec], years: List[int], word1: str, word2: str) -> \
        Tuple[
            np.ndarray, Tuple[Any, float]]:
    """

    @param models: list of w2v models
    @param years: list of years corresponding to each model
    @param word1: target word
    @param word2: word to compare to
    @return: cosine distances between the two words in each embedding and the Spearman correlation with time
    @summary: computes the pairwise similarity timeseries for two words in time distributed embedding models.

    usage example:
    pairwise_similarity_timeseries([model1, model2, model3], np.array([1900, 1950, 2000]), 'this', 'that')
    """
    sims = np.array([model.wv.cosine_similarities(model.wv.get_vector(word1), [model.wv.get_vector(word2)])[0] for model in models])


    spearman = scipy.stats.spearmanr(sims, years)
    return sims, spearman


def detect_known_shifts(models: List[gensim.models.Word2Vec], word: str, moving_from: List[str], moving_to: List[str],
                        years: List[int]) -> Tuple[int, int]:
    """

    @param models: list of w2v models
    @param word: target word
    @param moving_from: list of words that the target word is known to be moving away from
    @param moving_to: list of words that the target word is known to be moving toward
    @param years: list of years corresponding to each model
    @return: the average person correlation coefficient with time for distances between the target word and the words that it is moving from/to
    """
    time_sims_from = [pairwise_similarity_timeseries(models, years, word, moving_from_word) for moving_from_word in
                      moving_from]
    time_sims_to = [pairwise_similarity_timeseries(models, years, word, moving_to_word) for moving_to_word in moving_to]
    time_sims_to_average = np.mean([ts[1][0] for ts in time_sims_to])
    time_sims_from_average = np.mean([ts[1][0] for ts in time_sims_from])
    if time_sims_from_average < 0:
        print('%s is moving away from ' % word, moving_from)
    if time_sims_to_average > 0:
        print('%s is moving towards ' % word, moving_to)
    return int(time_sims_from_average), int(time_sims_to_average)


def discover_shifts(model_f: gensim.models.Word2Vec, model_t: gensim.models.Word2Vec, word_counts: Dict[str, int],
                    max_count: int, top_n_changed: int, top_n_neighbours: int) -> \
        Tuple[List[Tuple[str, float]], List[List[Tuple[str, float]]], List[List[Tuple[str, float]]]]:
    """

    @param model_f: the starting embedding model
    @param model_t: the embedding model that is used to measure change against
    @param word_counts: dictionary of words and word counts
    @param max_count: specify the maximum number of occurencies of a word to consider it, since the more frequent the word is the slower it changes its meaning
    @param top_n_changed: number of most changed words to return
    @param top_n_neighbours: number of neighbours to return for each most changed word in both the starting and ending models
    @return: most changed words and their displacement scores, closest words in the starting model, closets words in the target model
    @summary: This function automatically discovers the most shifted words between two embeddings and the words they shift from and to.

    example of usage:
    mc, fw, tw = discover_shifts(model1, model3, {'this': 5, 'is': 1, 'the': 3, 'sentence': 2}, 2, 2, 2)
    """
    filtered_vocab = [word for word, cnt in word_counts.items() if cnt <= max_count]
    displacements = [semantic_displacement([model_f, model_t], word)[0] for word in filtered_vocab]
    print(displacements)
    most_changed = [(filtered_vocab[i], displacements[i]) for i in
                    np.argpartition(displacements, -top_n_changed)[-top_n_changed:]]
    most_changed = list(sorted([a for a in most_changed], key=lambda tup: tup[1], reverse=True))
    from_words = [model_f.wv.most_similar(positive=word[0], topn=top_n_neighbours) for word in most_changed]
    to_words = [model_t.wv.most_similar(positive=word[0], topn=top_n_neighbours) for word in most_changed]
    return most_changed, from_words, to_words


def train_sgns(texts: List[List[List[str]]], embedding_dim: int, min_count: int, in_nb=True) -> List[gensim.models.Word2Vec]:
    """

    @param texts: List of lists of tokenized sentences grouped by time period
    @param embedding_dim: the embedding dimention for the vector space
    @param min_count: the minimum number of occurecncies of the word to include it in the embedding
    @param in_nb: use tqdm_notebook
    @return: list of gensim Word2Vec models that correspond to each time period
    @summary: this function takes in texts for different time periods and sequentially builds embedding models for each time period finetuning embeddings from the previous time period

    usage example:
    train _sgns([texts1, texts2, texts3], 128, 1)
    """
    models = []
    models.append(gensim.models.Word2Vec(
        texts[0],
        size=embedding_dim,
        window=5,
        min_count=min_count,
        workers=10,
        iter=10))
    t = tqdm_notebook if in_nb else tqdm
    for i in t(range(1, len(texts))):
        models[i - 1].save("./models/word2vec_%d.model" % i)
        models.append(gensim.models.Word2Vec.load("./models/word2vec_%d.model" % i))
        #models[i].wv.init_sims()
        models[i].build_vocab(texts[i], update=True)
        models[i].train(texts[i], total_examples=len(texts[i]), epochs=10)
    models[-1].save("./models/word2vec_%d.model" % (len(texts) - 1))
    return models
